import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import sys

import collections

from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import RobustScaler
from sklearn.preprocessing import MinMaxScaler
from keras.utils import to_categorical

import multiprocessing

## Util functions
def get_timeseries_by_column_name(train, passband, column_name):
    train_passband = train[train.passband == passband]
    ## This is where magic happens
    train_column_timeseries = train_passband.groupby(['object_id', 'passband'])[column_name].apply(lambda df: df.reset_index(drop=True)).unstack()
    train_column_timeseries.fillna(0, inplace=True)
    train_column_timeseries  = train_column_timeseries.reset_index()
    train_column_timeseries['feature_id'] = column_name
    return train_column_timeseries

def get_timeseries(train, passband):
    df = pd.concat([get_timeseries_by_column_name(train, passband, column_name) for column_name in ['mjd', 'flux', 'flux_err', 'detected']])
    df = df.sort_values(['object_id', 'passband'])
    return df

# Util function for padding
def pad_passband(passband, length):
    batch, d2, dim = passband.shape

    left_pad = (length - d2) // 2
    right_pad = length - d2 - left_pad

    padding = [(0, 0), (left_pad, right_pad), (0, 0)]

    passband = np.pad(passband, padding, mode='constant', constant_values=0)

    #
    window = (left_pad, d2 + left_pad)

    return passband, window


class PrepData:
    def __init__(self, *, data, feature_range=(-1,1)):
        prep_train_data = data.copy() # TODO : no need

        #
        self.rs = RobustScaler(with_scaling=False).fit(prep_train_data[['mjd', 'flux', 'flux_err', 'detected']])
        prep_train_data[['mjd', 'flux', 'flux_err', 'detected']] = self.rs.transform(prep_train_data[['mjd', 'flux', 'flux_err', 'detected']])

        #
        prep_train_data[['flux', 'flux_err']] = np.cbrt(prep_train_data[['flux', 'flux_err']])
        prep_train_data[['flux', 'flux_err']] = np.cbrt(prep_train_data[['flux', 'flux_err']])

        #prep_train_data.flux = np.cbrt(prep_train_data.flux)

        #
        self.mm = MinMaxScaler(feature_range=feature_range).fit(prep_train_data[['mjd', 'flux', 'flux_err', 'detected']])
        #
        #prep_train_data[['mjd', 'flux', 'flux_err']] = self.mm.transform(prep_train_data[['mjd', 'flux', 'flux_err']])

    def transform(self, data):
        prep_train_data = data.copy() # TODO : no need

        #
        prep_train_data[['mjd', 'flux', 'flux_err', 'detected']] = self.rs.transform(prep_train_data[['mjd', 'flux', 'flux_err', 'detected']])

        #
        prep_train_data[['flux', 'flux_err']] = np.cbrt(prep_train_data[['flux', 'flux_err']])
        prep_train_data[['flux', 'flux_err']] = np.cbrt(prep_train_data[['flux', 'flux_err']])

        #
        prep_train_data[['mjd', 'flux', 'flux_err', 'detected']] = self.mm.transform(prep_train_data[['mjd', 'flux', 'flux_err', 'detected']])

        return prep_train_data



# preprocessing 1 method
def prep_data(*, data, feature_range=(-1,1)):
    prep_train_data = data.copy() # TODO : no need

    #
    rs = RobustScaler(with_scaling=False)
    prep_train_data[['mjd', 'flux', 'flux_err', 'detected']] = rs.fit_transform(prep_train_data[['mjd', 'flux', 'flux_err', 'detected']])

    #
    prep_train_data[['flux', 'flux_err']] = np.cbrt(prep_train_data[['flux', 'flux_err']])
    prep_train_data[['flux', 'flux_err']] = np.cbrt(prep_train_data[['flux', 'flux_err']])

    #prep_train_data.flux = np.cbrt(prep_train_data.flux)

    #
    mm = MinMaxScaler(feature_range=feature_range)
    prep_train_data[['mjd', 'flux', 'flux_err', 'detected']] = mm.fit_transform(prep_train_data[['mjd', 'flux', 'flux_err', 'detected']])

    return prep_train_data


##
class __GetLabelsReturnType__(collections.namedtuple("__GetLabelsReturnType__", ["class_map", "target_map", "Y", "classes"])):
    pass

#
def get_labels(*, metadata, train_data_with_object_id):
    # List all classes
    classes = sorted(metadata.target.unique())

    #
    class_map = dict()
    for i,val in enumerate(classes):
        class_map[val] = i

    # We only need target for earch object_id, so using feature_id=='flux'
    #merged_train = train_list[0][train_list[0].feature_id == 'flux'].merge(train_metadata, on='object_id', how='left')
    merged_train = train_data_with_object_id.merge(metadata, on='object_id', how='left')
    merged_train = merged_train.drop(['ra',	'decl',	'gal_l',	'gal_b',	'ddf',	'hostgal_specz', \
            'hostgal_photoz',	'hostgal_photoz_err',	'distmod',	'mwebv'], axis=1)
    #merged_train.head()

    targets = merged_train.target
    #target_map = np.zeros((targets.shape[0],))
    target_map = np.array([class_map[val] for val in targets])
    Y = to_categorical(target_map)
    #Y.shape

    return __GetLabelsReturnType__(class_map, target_map, Y, classes)


def get_weigth_table(*, classes, target_map):
    y_count = collections.Counter(target_map)
    wtable = np.zeros((len(classes),))
    map_class_weight = {}
    for i in range(len(classes)):
        wtable[i] = y_count[i] / target_map.shape[0]
        map_class_weight[str(classes[i])] = wtable[i]
        

    return wtable, map_class_weight 


##
class __PrepForNetInputReturnType__(collections.namedtuple("__PrepForNetInputReturnType__", ["X_train", "train_list", "object_id"])):
    pass

def prep_worker(tsobj):
    timeseries_data = get_timeseries(tsobj[0], passband=tsobj[1])
    return (timeseries_data, tsobj[1])

#
def prep_for_net_input(prep_data):
    """
    """
    # Extract timeseries from pac_data
    """
    train_list = []
    shapes = []
    for passband in range(0, 6):
        train_passband =  get_timeseries(prep_data, passband=passband)
        train_list.append(train_passband)
        shapes.append(train_list[passband].shape)

        print("train_list[%d]" % passband, train_list[passband].shape)
    """

    train_list = []
    shapes = []
    dl = [(prep_data,0), (prep_data,1), (prep_data,2), (prep_data,3), (prep_data,4), (prep_data,5)]
    with multiprocessing.Pool() as pool:  
        results = pool.imap(prep_worker, dl)
        for res in results:
            train_list.append(res[0])
            shapes.append(res[0].shape)

            print("train_list[%d]" % res[1], res[0].shape)




    shapes = np.array(shapes)
    max_shape = np.max(shapes, axis=0)
    print(max_shape)

    #
    num_features = len(['mjd', 'flux', 'flux_err', 'detected'])
    drop_features= [ "feature_id", "object_id", "passband"]
    X_train_list = []

    for passband in range(0, 6):
        num_columns = len(train_list[passband].columns) - len(drop_features)
        print("num_columns[%d]:" % passband, num_columns)
        X_train_list.append(train_list[passband].drop(drop_features, axis=1).values.reshape(-1, num_features, num_columns).transpose(0, 2, 1))
        print("X_train_list[%d].shape:" % passband, X_train_list[passband].shape)


    # Pad and concatenate
    # TODO
    #max_timestamp_len = max([item.shape[1] for item in X_train_list])
    #print(max_timestamp_len)
    max_timestamp_len = 72
    for ind in range(len(X_train_list)):
        X_train_list[ind], wnd = pad_passband(X_train_list[ind], max_timestamp_len)
        print(X_train_list[ind].shape)

    # Reshape for autoencoder input
    X_train = np.concatenate(X_train_list, axis=2)
    X_train = X_train.reshape([int(max_shape[0] / 4), 72, -1, 4])

    #
    object_id = train_list[0][train_list[0].feature_id == "flux"].object_id.values

    return __PrepForNetInputReturnType__(X_train, train_list, object_id)


def aug_data(data, add_white_noise=True, white_noise_std=0.001):
    """
        Args:
            data - np.array of shape [batch, length, count, depth]
    """
    # Choose random cols and drop out them
    length = data.shape[1]
    nl = length * np.random.choice([0.7, 0.75 , 0.8, 0.85, 0.9, 0.95, 1.0],1, replace=False)
    samples = np.sort(np.random.choice(length, int(nl), replace=False))
    data = data[:,samples,:,:]

    # Pad
    _, d2, _, _ = data.shape

    left_pad = (length - d2) // 2
    right_pad = length - d2 - left_pad

    padding = [(0, 0), (left_pad, right_pad), (0, 0), (0,0)]

    data = np.pad(data, padding, mode='constant', constant_values=0)

    # white noise
    if add_white_noise:
        wn = np.random.normal(0, white_noise_std, size=data.shape)
        data += wn

    return data

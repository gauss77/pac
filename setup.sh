#!/usr/bin/env bash

#set -e

sudo apt install python-pip
sudo apt install python3-pip

sudo pip install --upgrade virtualenv     
virtualenv -p python3 .env
source .env/bin/activate        
pip3 install --upgrade -r requirements.txt

#pip3 install --upgrade tensorflow-gpu
#pip3 install --upgrade tensorflow # In case of CPU

#sudo apt install python3-tk

# build if necessary
#mkdir pycocotools
#cd ./coco/PythonAPI 
#make
#cp -r pycocotools/* ../../pycocotools
#cd -

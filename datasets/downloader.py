import os as os
import wget  as wget
import zipfile as zipfile
import collections as collections

# Info needed for receive resources from net.
Resource = collections.namedtuple("Resource", ["url", "out_dir"])

# COCO 2014 dataset.
coco_dataset_2014 = {\
                "train_images" : Resource("http://images.cocodataset.org/zips/train2014.zip", None)\
                , "val_images" : Resource("http://images.cocodataset.org/zips/val2014.zip", None) \
                , "test_images" : Resource("http://images.cocodataset.org/zips/test2014.zip", None)\
                , "train_val_annotations" : Resource("http://images.cocodataset.org/annotations/annotations_trainval2014.zip", None) \
                , "testing_image_info" : Resource("http://images.cocodataset.org/annotations/image_info_test2014.zip", None) \
                , "minival" : Resource("https://dl.dropboxusercontent.com/s/o43o90bna78omob/instances_minival2014.json.zip?dl=0", "annotations")
                , "validation_minus_minival" : Resource("https://dl.dropboxusercontent.com/s/s3tw5zcg7395368/instances_valminusminival2014.json.zip?dl=0", "annotations")
                }

# pac dataset.
pac_dataset = {\
                "training_set" : Resource("https://www.kaggle.com/c/PLAsTiCC-2018/download/training_set.csv", None)\
                , "training_set_metadata" : Resource("https://www.kaggle.com/c/PLAsTiCC-2018/download/training_set_metadata.csv", None)\
                }



def download(name, resource):
    """ Downloads resource from net.

    param name : resource name
    resource   : variable of type Resource
    """
    
    # Download if doesn't exists
    if os.path.exists(os.path.basename(resource.url)):
        print ( " Path %s already exists." % (os.path.basename(resource.url)) )
        return
    else:
        print( " Downloading %s " % (name) )
        f_name = wget.download(resource.url)
        print( "\n Extracting %s" % (f_name) )

    # Extraction
    #with zipfile.ZipFile(f_name) as tmp_file:
    #    tmp_file.extractall("./" if not resource.out_dir else resource.out_dir)


if __name__ == "__main__":
    import argparse as argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(description = "Dataset Downloader")
    parser.add_argument("--name", default=None)
    args = parser.parse_args()

    # 
    if args.name == None:
        for name, url in pac_dataset.items():
            download(name, url)
    else:
        if args.name in pac_dataset:
            download(args.name, pac_dataset[args.name])
        else:
            print("Pac dataset doesn't contain resource by name %s" % (args.name) )

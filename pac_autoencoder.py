from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D
from keras.layers import Flatten, Dropout, Concatenate
from keras.layers import Reshape, Conv2DTranspose
from keras import regularizers


from keras.layers.advanced_activations import LeakyReLU
from keras_contrib.layers.normalization import InstanceNormalization
from keras.models import Sequential, Model
from sklearn.model_selection import StratifiedKFold

from keras.optimizers import Adam
from keras.models import Model
from keras import backend as K

import tensorflow as tf


import time
import datetime
import numpy as np

import os
import re

import pac_dataset as pac_data


class Config:
    def __init__(self):
        #
        self.batch_size = 1024
        self.epochs = 1000

        #
        self.latent_dim = 14
        self.input_shape = (72,6,4)

        #
        self.desc_reg_weight_decay = 0.0001
        self.ae_reg_weight_decay = 0.0001




#####
#####
class pac_autoencoder:
    def __init__(self, *, model_dir, weight_table=None):

        def mywloss(y_true,y_pred):  
            yc=tf.clip_by_value(y_pred,1e-15,1-1e-15)
            loss=-(tf.reduce_mean(tf.reduce_mean(y_true*tf.log(yc),axis=0)/weight_table))
            return loss

        # SETUP
        #
        self.batch_size = 512
        self.epochs = 1000
        self.model_dir = model_dir
        self.set_log_dir()

        
        #
        self.latent_dim = 14
        self.input_shape = (72,6,4)

        # Autoencoder
        #
        self.autoencoder = self.build_autoencoder()
        

        # Discriminator

        # Build and compile the discriminator
        self.discriminator = self.build_discriminator()
        self.compile_descriminator()

        # Build Combined Generator
        #
        input_img = Input(shape=self.input_shape)
        #
        #encoded_repr = self.encoder(input_img)
        reconstructed_curve, encoded_curve = self.autoencoder(input_img)

        print(reconstructed_curve.shape)
        print(encoded_curve.shape)
        

        # For the adversarial_autoencoder model we will only train the generator
        self.discriminator.trainable = False
        validity = self.discriminator(reconstructed_curve)

        # Build Classificator
        #optimizer = Adam(lr=0.001, beta_1=0.9, beta_2=0.999)#
        #early_stopping = EarlyStopping(monitor='loss', patience=30, verbose=1)
        """
        model.compile(loss=mywloss, optimizer=optimizer, metrics=['accuracy'])
        history = model.fit([x_train0, x_train1, x_train2, x_train3, x_train4, x_train5], y_train,
                        validation_data=[[x_valid0, x_valid1, x_valid2, x_valid3, x_valid4, x_valid5], y_valid],
                        epochs=epochs,
                            batch_size=batch_size,
                        shuffle=True,verbose=1,callbacks=[checkPoint, early_stopping]) 
        """
        self.classificator = self.build_classificator(len(weight_table))
        probs = self.classificator(encoded_curve)

        
        # The discriminator determines validity of the encoding
        #optimizer = Adam(lr=0.001, beta_1=0.9, beta_2=0.999)  #    0.0002, 0.5)
        #self.autoencoder.compile(loss=['mse'], optimizer=optimizer)
        
        # The adversarial_autoencoder model  (stacked generator and discriminator)
        self.adversarial_autoencoder = Model(input_img, [reconstructed_curve, validity, probs])
        self.compile_adversarial_autoencoder(classificator_loss=mywloss)

    def compile_descriminator(self):
        # Add L2 Regularization
        # Skip gamma and beta weights of batch normalization layers.
        weight_decay = 0.0001
        reg_losses = [
            regularizers.l2(weight_decay)(w) / tf.cast(tf.size(w), tf.float32)
            for w in self.discriminator.trainable_weights
            if 'gamma' not in w.name and 'beta' not in w.name]

        self.discriminator.add_loss(tf.add_n(reg_losses))

        #
        optimizer = Adam(0.0002, 0.5, clipnorm=1.0)
        self.discriminator.compile(loss='mse',
            optimizer=optimizer,
            metrics=['accuracy'])



    def compile_adversarial_autoencoder(self, *, classificator_loss):
        # Add L2 Regularization
        # Skip gamma and beta weights of batch normalization layers.
        weight_decay = 0.0001
        reg_losses = [
            regularizers.l2(weight_decay)(w) / tf.cast(tf.size(w), tf.float32)
            for w in self.adversarial_autoencoder.trainable_weights
            if 'gamma' not in w.name and 'beta' not in w.name]

        self.adversarial_autoencoder.add_loss(tf.add_n(reg_losses))


        #
        self.adversarial_autoencoder.compile(loss=['mae', 'mse', classificator_loss],
            loss_weights=[1, 1, 1],
            optimizer=Adam(lr=0.001, beta_1=0.9, beta_2=0.999, clipnorm=1.0))

        
    
    def build_encoder(self, shape=(72,6,4), return_model=False):
        #
        self.gf = 32

        #
        def conv2d(layer_input, filters, f_size=4, strides=2, name=""):
            """Layers used during downsampling"""
            d = Conv2D(filters, kernel_size=f_size, strides=strides, padding='same', name=name+"_Conv2D")(layer_input)
            d = LeakyReLU(alpha=0.2, name = name + "_LRelu")(d)
            d = InstanceNormalization(name = name + "_INor")(d) # TODO
            return d
        
        input_img = Input(shape=shape)  # adapt this if using `channels_first` image data format
        
        # Downsampling
        d1 = conv2d(input_img, self.gf, f_size=(5,1), strides=(2,1), name="encoder_e1")
        d2 = conv2d(d1, self.gf*2, f_size=(5,1), strides=(2,1), name="encoder_e2")
        d3 = conv2d(d2, self.gf*4, f_size=(4,4), strides=(2,1), name="encoder_e3")
        #x = conv2d(x, self.gf*8)

        self.d0, self.d1, self.d2, self.d3 = input_img, d1, d2, d3
        
        #
        #x = Conv2D(8, (3, 1), activation='relu', padding='valid')(input_img)
        #x = Conv2D(16, (3, 1), activation='relu', padding='valid')(x)
        #x = Conv2D(32, (3, 1), activation='relu', padding='valid')(x)
        #x = Conv2D(64, (3, 1), activation='relu', padding='valid')(x)
        #print(x.shape)
        #x = MaxPooling2D((2, 1), padding='same')(x)
        #x = Conv2D(32, (3, 1), activation='relu', padding='valid')(x)
        #x = Conv2D(32, (3, 1), activation='relu', padding='valid')(x)
        #x = MaxPooling2D((2, 2), padding='same')(x)
        #print(x.shape)
        #x = Conv2D(32, (3, 3), activation='relu', padding='valid')(x)
        #x = Conv2D(32, (3, 1), activation='relu', padding='valid')(x)
        #x = Conv2D(32, (2, 1), activation='relu', padding='valid')(x)
        #x = Conv2D(32, (3, 3), activation='relu', padding='same')(x)
        #encoded = Conv2D(14, (9,1), activation='relu', padding='valid')(x)
        
        #encoded = MaxPooling2D((2, 2), padding='same')(x)
        
        self.encoded = self.d3
        self.enc_shape = self.encoded.shape.as_list()[1:]

        print(self.encoded.shape)
        
        if return_model: 
            return Model(input_img, self.encoded)
    
    def build_decoder(self):

        self.out_ch = 4
        
        def deconv2d(layer_input, skip_input, filters, *, strides, f_size=4, dropout_rate=0, name=""):
            """Layers used during upsampling"""
            u = UpSampling2D(size=strides, name = name + "_UpS2D")(layer_input)
            u = Conv2D(filters, kernel_size=f_size, strides=1, padding='same', activation='relu', name = name + "_Conv2D")(u)
            if dropout_rate:
                u = Dropout(dropout_rate, name = name + "_Dropout")(u)
            u = InstanceNormalization(name = name + "_INor")(u)
            u = Concatenate(name = name + "_Conc")([u, skip_input])
            return u
        
        #enc_rep = Input(shape=self.enc_shape) 

        # Upsampling
        u1 = deconv2d(self.encoded, self.d2, self.gf*2, f_size=(3,3), strides=(2,1), name="decoder_u1")
        u2 = deconv2d(u1, self.d1, self.gf, f_size=(5,1), strides=(2,1), name="decoder_u2")
        #u3 = deconv2d(u2, d1, self.gf)

        u3 = UpSampling2D(size=(2,1), name="decoder_u3_UpS2D")(u2)
        print("u3 shape : {}".format(u3.shape))
        decoded = Conv2D(self.out_ch, kernel_size=4, strides=1, padding='same', activation='tanh', name="decoder_d_Conv2D")(u3)

        self.u1, self.u2, self.u3 = u1, u2, u3
        self.decoded = decoded
        # enc_rep = Input(shape=self.enc_shape) 
   
        # x = Conv2DTranspose(2, kernel_size=3, strides=(2,2), activation='relu', padding='same')(enc_rep)
        # x = Conv2DTranspose(2, kernel_size=3, strides=(2,3), activation='relu', padding='same')(x)
        # x = Conv2DTranspose(2, kernel_size=3, strides=(3,1), activation='relu', padding='same')(x)
        # x = Conv2DTranspose(2, kernel_size=3, strides=(3,1), activation='relu', padding='same')(x)
     
        # x = Conv2D(8, (3, 1), activation='relu', padding="same")(x)
        # x = UpSampling2D((2, 1))(x)
        #decoded = Conv2DTranspose(2, kernel_size=3, strides=2, activation='tanh', padding='same')(x)
        # decoded = Conv2D(3, (3, 1), activation='tanh', padding='same')(x)
        
        self.dec_shape = decoded.shape.as_list()[1:]

        print("decoded shape : {}".format(self.decoded.shape))
    
        return Model(self.d0, [decoded, self.encoded])

    def build_autoencoder(self):
        self.encoder = self.build_encoder()
        return self.build_decoder()

    def build_discriminator(self):
        self.df = 64

        def d_layer(layer_input, filters, *, f_size=4, strides=2, normalization=True, name=""):
            """Discriminator layer"""
            d = Conv2D(filters, kernel_size=f_size, strides=strides, padding='same', name = name + "_Conv2D")(layer_input)
            d = LeakyReLU(alpha=0.2, name = name + "_LRelu")(d)
            if normalization:
                d = InstanceNormalization(name = name + "_INor")(d)
            return d

        img = Input(shape=self.dec_shape)

        d1 = d_layer(img, self.df, f_size=(5,1), strides=(2,1), normalization=False, name="discriminator_d1")
        d2 = d_layer(d1, self.df*2, f_size=(5,1), strides=(2,1), name="discriminator_d2")
        print("descriminator d2 shape : {}".format(d2.shape))
        d3 = d_layer(d2, self.df*4, f_size=(4,4), strides=(2,1), name="discriminator_d3")
        print("descriminator d3 shape : {}".format(d3.shape))
        #d4 = d_layer(d3, self.df*8)

        validity = Conv2D(1, kernel_size=4, strides=1, padding='same', name="discriminator_val")(d3)
        print("descriminator validity shape : {}".format(validity.shape))

        self.disc_patch = validity.shape.as_list()[1:]

        return Model(img, validity)
    

    def build_discriminator_midpoint(self):

        model = Sequential()

        model.add(Reshape((self.latent_dim,), input_shape=(1,1,self.latent_dim)))
        model.add(Dense(512)) #, input_dim=self.latent_dim)
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dense(256))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dense(1, activation="sigmoid"))
        model.summary()

        encoded_repr = Input(shape=(1, 1, self.latent_dim, ))
        validity = model(encoded_repr)

        return Model(encoded_repr, validity)

    def build_classificator(self, n_classes):
        #n_classes = len(classes)

        def conv2d(layer_input, filters, f_size=4, strides=2, name=""):
            """Layers used during downsampling"""
            d = Conv2D(filters, kernel_size=f_size, strides=strides, padding='valid', name = name + "_Conv2D")(layer_input)
            d = LeakyReLU(alpha=0.2, name = name + "_LRelu")(d)
            d = InstanceNormalization(name = name + "_INor")(d) # TODO
            return d

        h, w, ch = self.enc_shape
        enc_inp = Input(shape=self.enc_shape)

        c1 = conv2d(enc_inp, ch, f_size=(h,w), strides=(1,1), name="classificator_c1")
        c2 = conv2d(c1, ch, f_size=(1,1), strides=(1,1), name="classificator_c2")

        print("classificator shape c2 : {}".format(c2.shape))
        c2 = Reshape((ch,), input_shape=(1,1,ch), name="classificator_c2_reshape")(c2)
        print("classificator shape rc2 : {}".format(c2.shape))

        print(n_classes)
        probs = Dense(n_classes, activation='softmax', name="classificator_probs")(c2)

        return Model(enc_inp, probs)


        """
        def weight_variable(shape, name=None):
            return np.random.normal(scale=.01, size=shape)

        def build_model():
            def basic_layer(input_):
                output = GRU(64,
                             kernel_initializer=weight_variable,
                             dropout=0.5,
                             recurrent_dropout=0.5,
                             return_sequences=True)(input_)
                output = Dropout(0.5)(output)
                output = GRU(32, return_sequences=True, dropout=0.5, recurrent_dropout=0.5)(output)
                output = GRU(16, dropout=0.5, recurrent_dropout=0.5)(output)
                output = Dense(32)(output)
                return output
            
            # Keras functional API supports multiple inputs!
            input0 = Input(shape=(None, num_features), dtype='float32', name='passband0')
            input1 = Input(shape=(None, num_features), dtype='float32', name='passband1')
            input2 = Input(shape=(None, num_features), dtype='float32', name='passband2')
            input3 = Input(shape=(None, num_features), dtype='float32', name='passband3')
            input4 = Input(shape=(None, num_features), dtype='float32', name='passband4')
            input5 = Input(shape=(None, num_features), dtype='float32', name='passband5')
            
            merged_output = concatenate([basic_layer(input0),basic_layer(input1),basic_layer(input2),basic_layer(input3),basic_layer(input4),basic_layer(input5)])
            merged_output = Dense(64)(merged_output)
            final_output = Dense(len(classes), activation='softmax')(merged_output)
            return Model(inputs=[input0, input1, input2, input3, input4, input5], outputs=[final_output])
        """

    def save(self):
        chk_path = self.checkpoint_path.replace("*epoch*", "{:04d}".format(self.epoch))

        self.adversarial_autoencoder.save_weights(chk_path)

    def load_weights(self, filepath):
        self.adversarial_autoencoder.load_weights(filepath) #, by_name=True)
        self.set_log_dir(filepath)

    def set_log_dir(self, model_path=None):
        self.epoch = 0
        now = datetime.datetime.now()

        # If we have a model path with date and epochs use them
        if model_path:
            # Continue from we left of. Get epoch and date from the file name
            # A sample model path might look like:
            # \path\to\logs\coco20171029T2315\mask_rcnn_coco_0001.h5 (Windows)
            # /path/to/logs/coco20171029T2315/mask_rcnn_coco_0001.h5 (Linux)
            regex = r".*[/\\][\w-]+(\d{4})(\d{2})(\d{2})T(\d{2})(\d{2})[/\\]mask\_rcnn\_[\w-]+(\d{4})\.h5"
            m = re.match(regex, model_path)
            if m:
                now = datetime.datetime(int(m.group(1)), int(m.group(2)), int(m.group(3)),
                                        int(m.group(4)), int(m.group(5)))
                # Epoch number in file is 1-based, and in Keras code it's 0-based.
                # So, adjust for that then increment by one to start from the next epoch
                self.epoch = int(m.group(6)) - 1 + 1
                print('Re-starting from epoch %d' % self.epoch)

        # Directory for training logs
        self.log_dir = os.path.join(self.model_dir, "{}{:%Y%m%dT%H%M}".format("pac", now))
        if not os.path.isdir(self.log_dir):
            os.mkdir(self.log_dir)

        # Path to save after each epoch. Include placeholders that get filled by Keras.
        self.checkpoint_path = os.path.join(self.log_dir, "pac_{}_*epoch*.h5".format(1))# , self.epoch))
        #self.checkpoint_path = self.checkpoint_path.replace("*epoch*", "{epoch:04d}")


    
    def train(self, *, X, y_map, y_categorical):
        #
        #y_map = target_map
        #y_categorical = Y
        
        
        #
        start = time.time()

        #
        # folds = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
        # trn_, val_ = next(folds.split(y_map, y_map))
        #
        # x_train, y_train = X_train[trn_], y_categorical[trn_]
        # x_valid, y_valid = X_train[val_], y_categorical[val_]
        
        #
        #print(x_train.shape)
        #self.autoencoder.fit(x_train, x_train[:,:,:,0:3],
        #         epochs=self.epochs,
        #          batch_size=self.batch_size,
        #          shuffle=True,
        #          validation_data=(x_valid, x_valid[:,:,:,0:3]),
        #          callbacks=[TensorBoard(log_dir='/tmp/autoencoder')])

        folds = StratifiedKFold(n_splits=5, shuffle=True, random_state=1)
        trn_, val_ = next(folds.split(y_map, y_map))

        x_train, y_train = X[trn_], y_categorical[trn_]
        x_valid, y_valid = X[val_], y_categorical[val_]
        
        # Adversarial ground truths
        #valid_mid = np.ones((self.batch_size, 1))
        #fake_mid = np.zeros((self.batch_size, 1))

        # Adversarial loss ground truths
        train_ones = np.ones([self.batch_size] + self.disc_patch)
        train_zeros = np.zeros([self.batch_size] + self.disc_patch)

        #
        validation_ones = np.ones([val_.shape[0]] + self.disc_patch)
        validation_zeros = np.zeros([val_.shape[0]] + self.disc_patch)

        
        for ep in range(self.epoch, self.epochs):
            
            steps = 5 # int(X.shape[0] / self.batch_size)
            
            d_loss = 100
            g_loss = 100
            
            for i in range(steps): 

                # ---------------------
                #  Train Discriminator
                # ---------------------

                # Select a random batch of images
                idx = np.random.randint(0, x_train.shape[0], self.batch_size)
                real_curves, y = x_train[idx], y_train[idx]

                real_curves = pac_data.aug_data(real_curves)

                # 
                fake_curves, encoded_curve = self.autoencoder.predict(real_curves)
                #latent_real = np.random.normal(size=(self.batch_size, self.latent_dim))
                #latent_real = latent_real.reshape([self.batch_size, 1, 1, self.latent_dim])
                
                # Train the discriminator
                d_loss_real = self.discriminator.train_on_batch(real_curves, train_ones)
                d_loss_fake = self.discriminator.train_on_batch(fake_curves, train_zeros)
                d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)
                
                # ---------------------
                #  Train Generator
                # ---------------------
                
                # Train the generator
                g_loss = self.adversarial_autoencoder.train_on_batch(real_curves, [real_curves, train_ones, y])


            self.epoch += 1
            self.save()

            # Plot the progress
            print ("%d [D loss: %f, acc: %.2f%%] [G loss: %f, mse: %f, mywloss: %f]" % (ep, d_loss[0], 100*d_loss[1], g_loss[0], g_loss[1], g_loss[2]))


            # validation

            fake_curves, encoded_curve = self.autoencoder.predict(x_valid)
            #latent_real = np.random.normal(size=(self.batch_size, self.latent_dim))
            #latent_real = latent_real.reshape([self.batch_size, 1, 1, self.latent_dim])
            
            # Train the discriminator
            d_loss_real = self.discriminator.train_on_batch(x_valid, validation_ones)
            d_loss_fake = self.discriminator.train_on_batch(fake_curves, validation_zeros)
            d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)


            g_val_loss = self.adversarial_autoencoder.test_on_batch(x_valid, [x_valid, validation_ones, y_valid])
            print(" Validation ")
            print ("%d [D loss: %f, acc: %.2f%%] [G loss: %f, mse: %f, mywloss: %f]" % (ep, d_loss[0], 100*d_loss[1], g_val_loss[0], g_val_loss[1], g_val_loss[2]))
                



def multi_weighted_logloss(y_ohe, y_p):
    """
    @author olivier https://www.kaggle.com/ogrellier
    multi logloss for PLAsTiCC challenge
    """
    classes = [6, 15, 16, 42, 52, 53, 62, 64, 65, 67, 88, 90, 92, 95]
    class_weight = {6: 1, 15: 2, 16: 1, 42: 1, 52: 1, 53: 1, 62: 1, 64: 2, 65: 1, 67: 1, 88: 1, 90: 1, 92: 1, 95: 1}
    # Normalize rows and limit y_preds to 1e-15, 1-1e-15
    y_p = np.clip(a=y_p, a_min=1e-15, a_max=1-1e-15)
    # Transform to log
    y_p_log = np.log(y_p)
    # Get the log for ones, .values is used to drop the index of DataFrames
    # Exclude class 99 for now, since there is no class99 in the training set 
    # we gave a special process for that class
    y_log_ones = np.sum(y_ohe * y_p_log, axis=0)
    # Get the number of positives for each class
    nb_pos = y_ohe.sum(axis=0).astype(float)
    # Weight average and divide by the number of positives
    class_arr = np.array([class_weight[k] for k in sorted(class_weight.keys())])
    y_w = y_log_ones * class_arr / nb_pos    
    loss = - np.sum(y_w) / np.sum(class_arr)
    return loss

def multi_weighted_logloss_(y_ohe, y_p, class_weight):
    """
    @author olivier https://www.kaggle.com/ogrellier
    multi logloss for PLAsTiCC challenge
    """
    #classes = [6, 15, 16, 42, 52, 53, 62, 64, 65, 67, 88, 90, 92, 95]
    #class_weight = {6: 1, 15: 2, 16: 1, 42: 1, 52: 1, 53: 1, 62: 1, 64: 2, 65: 1, 67: 1, 88: 1, 90: 1, 92: 1, 95: 1}
    # Normalize rows and limit y_preds to 1e-15, 1-1e-15
    y_p = np.clip(a=y_p, a_min=1e-15, a_max=1-1e-15)
    # Transform to log
    y_p_log = np.log(y_p)
    # Get the log for ones, .values is used to drop the index of DataFrames
    # Exclude class 99 for now, since there is no class99 in the training set 
    # we gave a special process for that class
    y_log_ones = np.sum(y_ohe * y_p_log, axis=0)
    # Get the number of positives for each class
    nb_pos = y_ohe.sum(axis=0).astype(float)
    # Weight average and divide by the number of positives
    wk = sorted(class_weight.keys())[:y_p.shape[1]]
    class_arr = np.array([class_weight[k] for k in wk])
    y_w = y_log_ones  * class_arr / nb_pos    
    loss = - np.sum(y_w) / np.sum(class_arr)
    return loss

# 
"""
def mywloss(y_true,y_pred):  
    yc=tf.clip_by_value(y_pred,1e-15,1-1e-15)
    loss=-(tf.reduce_mean(tf.reduce_mean(y_true*tf.log(yc),axis=0)/wtable))
    return loss
"""

def plot_loss_acc(history):
    plt.plot(history.history['loss'][1:])
    plt.plot(history.history['val_loss'][1:])
    plt.title('model loss')
    plt.ylabel('val_loss')
    plt.xlabel('epoch')
    plt.legend(['train','Validation'], loc='upper left')
    plt.show()
    
    plt.plot(history.history['acc'][1:])
    plt.plot(history.history['val_acc'][1:])
    plt.title('model Accuracy')
    plt.ylabel('val_acc')
    plt.xlabel('epoch')
    plt.legend(['train','Validation'], loc='upper left')
    plt.show()
